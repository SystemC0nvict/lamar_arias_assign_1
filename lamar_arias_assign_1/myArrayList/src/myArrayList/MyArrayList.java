package MyArrayList;
import java.util.Arrays;
import java.lang.reflect.Array;
public class MyArrayList{
    private int[] numl;
    
    public MyArrayList(){
        numl = new int[50];//intializes array size
    }
    
    void insertSorted(int NewValue){
        int siz = Array.getLength(numl);//gets size of array
        if(numl[siz] != 0){//makes sure last array slot isnt null
            for(int i = 0; i < siz; i++){
                if(numl[i] == 0){//chacks for null element to insert at
                    numl[i] = NewValue;//insert element
                    i = siz;//breaks loop
                }
            }
        }
        else{//else loop is to increase array size if array is full
            copyarr(numl);
             for(int i = 0; i < siz + 1; i++){
                if(numl[i] == 0){
                    numl[i] = NewValue;
                    i = siz;
                }
            }
        }
        Arrays.sort(numl);//sorts array after inserting
        
    }
    
    void copyarr(int[] old){//function to increase size of array and copy elements over
        int siz = Array.getLength(old);
        int newsiz = siz * 3 / 2;
        int[] news = new int[siz];//creatse new array that is 1.5 times larger
        for(int i = 0; i<siz; i++){
            news[i] = old[i];//copies elements over
        }
        Arrays.sort(news);
        numl =  news.clone();
    }
    
    void removeValue(int value){
        int siz = Array.getLength(numl);
        for(int i = 0; i <siz; i++){
            if(numl[i] == value){
                numl[i] = 0;
            }
        }
        Arrays.sort(numl);
    }
    
    int size(){//gets total number of valid elements in the array
        int siz = Array.getLength(numl);
        int count = 0;
        for(int i = 0; i < siz; i++){
            if(numl[i] != 0){
                count++;
            }
        }    
        return count;
    }
    
    int sum(){//calculates sum of valid elements in the array
        int siz = Array.getLength(numl);
        int sum = 0;
        for(int i = 0; i < siz; i++){
            if(numl[i] != 0){
                sum += numl[i];
            }
        }
        return sum;
    }
    
    public String toString(){//prints elements in string format
        int siz = Array.getLength(numl);
        String tmp = "tmp";
        for(int i = 0; i < siz; i++){
            if(numl [i] != 0){
                System.out.printf("%d /n", numl[i]);//outputs array elements with new lines between them
            }
        }
        return tmp;
    }
    
}
