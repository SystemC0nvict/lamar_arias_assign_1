public Class MyArrayList{
    private int[] numl;
    
    public MyArrayList(){
        numl = new int[50];//intializes array size
    }
    
    void insertSorted(int NewValue){
        int siz = Array.getlength(numl);//gets size of array
        if(numl[siz]) != NULL){//makes sure last array slot isnt null
            for(int i = 0; i < siz; i++){
                if(numl[i] == NULL){//chacks for null element to insert at
                    numl[i] = NewValue;//insert element
                    i = siz;//breaks loop
                }
            }
        }
        else{//else loop is to increase array size if array is full
            MyArrayList.copyarr(numl);
             for(int i = 0; i < siz + 1; i++){
                if(numl[i] == NULL){
                    numl[i] = NewValue;
                    i = siz;
                }
            }
        }
        Array.sort(numl);//sorts array after inserting
        
    }
    
    void copyarr(int[] old){//function to increase size of array and copy elements over
        int siz = Array.getlength(old);
        int[] new = new int[siz*1.5];//creatse new array that is 1.5 times larger
        for(int i = 0;, i<siz; i++){
            new[i] = old[i];//copies elements over
        }
        Array.sort(new);
        numl =  new.clone();
    }
    
    void removeValue(int value){
        int siz = Array.getlength(numl);
        for(int i = 0; i <siz; i++){
            if(numl[i] == value){
                numl[i] = NULL;
            }
        }
        Array.sort(numl);
    }
    
    int size(){//gets total number of valid elements in the array
        int siz = Array.getlength(numl);
        int count = 0;
        for(int i = 0; i < siz; i++){
            if(numl[i] != NULL){
                count++;
            }
        }    
        return count;
    }
    
    int sum(){//calculates sum of valid elements in the array
        int siz = Array.getlength(numl);
        int sum = 0;
        for(int i = 0; i < siz; i++){
            if(numl[i] != NULL){
                sum += numl[i];
            }
        }
        return sum;
    }
    
    void toString(){//prints elements in string format
        int siz = Array.getlength(numl);
        for(int i = 0; i < siz; i++){
            if(numl [i] != NULL){
                System.out.print("%d /n", numl[i]);//outputs array elements with new lines between them
            }
        }
    }
    
}