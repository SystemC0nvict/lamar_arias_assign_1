public Class MyArrayList{
    private int[] numl;
    
    public MyArrayList(){
        numl = new int[50];
    }
    
    void insertSorted(int NewValue){
        int siz = Array.getlength(numl);
        if(numl[siz]) != NULL){
            for(int i = 0; i < siz; i++){
                if(numl[i] == NULL){
                    numl[i] = NewValue;
                    i = siz;
                }
            }
        }
        else{
            MyArrayList.copyarr(numl);
             for(int i = 0; i < siz + 1; i++){
                if(numl[i] == NULL){
                    numl[i] = NewValue;
                    i = siz;
                }
            }
        }
        Array.sort(numl);
        
    }
    
    void copyarr(int[] old){
        int siz = Array.getlength(old);
        int[] new = new int[siz*1.5];
        for(int i = 0;, i<siz; i++){
            new[i] = old[i];
        }
        Array.sort(new);
        numl =  new.clone();
    }
    
    void removeValue(int value){
        int siz = Array.getlength(numl);
        for(int i = 0; i <siz; i++){
            if(numl[i] == value){
                numl[i] = NULL;
            }
        }
        Array.sort(numl);
    }
    
    int size(){
        int siz = Array.getlength(numl);
        int count = 0;
        for(int i = 0; i < siz; i++){
            if(numl[i] != NULL){
                count++;
            }
        }    
        return count;
    }
    
    int sum(){
        int siz = Array.getlength(numl);
        int sum = 0;
        for(int i = 0; i < siz; i++){
            if(numl[i] != NULL){
                sum += numl[i];
            }
        }
        return sum;
    }
    
    void toString(){
        int siz = Array.getlength(numl);
        for(int i = 0; i < siz; i++){
            if(numl [i] != NULL){
                System.out.print("%d /n", numl[i]);
            }
        }
    }
    
}